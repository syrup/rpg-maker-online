var createBox;

( function() {
	function box( ctx, x, y, w, h ) {
		var g = graphic[config.system.skin];

		x *= config.tile.width;
		y *= config.tile.height;
		w *= config.tile.width;
		h *= config.tile.height;

		ctx.drawImage(g, 0, 0, 128, 128, x+3, y+3, w-6, h-6 ); //bg

		ctx.drawImage(g, 128,  0, 16, 16, x,			y,			16, 16); //links oben
		ctx.drawImage(g, 176,  0, 16, 16, x+w-16,	y,			16, 16); //rechts oben
		ctx.drawImage(g, 128, 48, 16, 16, x,			y+h-16,	16, 16); //links unten
		ctx.drawImage(g, 176, 48, 16, 16, x+w-16, y+h-16,	16, 16); //rechts unten

		for( var rx=x+16; rx<x+w-16; rx+=32 ){
		 ctx.drawImage(g, 144,  0, 32, 16, rx, y,				32, 16); // oben
		 ctx.drawImage(g, 144, 48, 32, 16, rx, y+h-16,	32, 16); // unten
		}

		for(var ry=y+16; ry<y+h-16; ry+=32 ){
		 ctx.drawImage(g, 128, 16, 16, 32, x,				ry, 16, 32); // links
		 ctx.drawImage(g, 176, 16, 16, 32, x+w-16,	ry, 16, 32); // right
		}
	}

	function roundRectPath( ctx, x, y, w, h, r ) {
		ctx.beginPath();
		ctx.moveTo(x + r, y);
		ctx.lineTo(x + w - r, y);
		ctx.quadraticCurveTo(x + w, y, x + w, y + r);
		ctx.lineTo(x + w, y + h - r);
		ctx.quadraticCurveTo(x + w, y + h, x + w - r, y + h);
		ctx.lineTo(x + r, y + h);
		ctx.quadraticCurveTo(x, y + h, x, y + h - r);
		ctx.lineTo(x, y + r);
		ctx.quadraticCurveTo(x, y, x + r, y);
		ctx.closePath();
	}

	createBox = function( w, h ) {
		var canvas = document.createElement('canvas');
		var ctx = canvas.getContext('2d');

		canvas.width = w*config.tile.width;
		canvas.height = h*config.tile.height;

		box( ctx, 0, 0, w, h );
		return canvas;
	}

	game.createBuffer = function( game, display ) {
		this.display = document.getElementById('gameframe');
		this.displayCtx = this.display.getContext('2d');

		this.buffer = document.createElement('canvas');
		this.bufferCtx = this.buffer.getContext('2d');
		this.buffer.width = this.display.width;
		this.buffer.height = this.display.height;

		this.bufferCtx.drawBox = function( x, y, w, h ) {
			box( this, x, y, w, h );
		}

		this.bufferCtx.strokeRoundRect = function( x, y, w, h, r ) {
			roundRectPath( this, x, y, w, h, r );
			this.stroke();
		}

		this.bufferCtx.fillRoundRect = function( x, y, w, h, r ) {
			roundRectPath( this, x, y, w, h, r );
			this.fill();
		}

		this.bufferCtx.progressBar = function( p, x, y, w, h, r, colors ) {
			var grad = this.createLinearGradient(x,y,x,y+h);
			grad.addColorStop(  0, colors[0] );
			grad.addColorStop( .7, colors[1] );

			this.fillStyle = grad;
			this.fillRoundRect( x, y, w * p, h, r );

			//this.lineWidth = 2;
			this.strokeStyle = config.system.text;
			this.strokeRoundRect( x, y, w, h, r );
		}

		this.bufferCtx.oldBox = function( x, y, w, h ) {
			this.fillStyle = "rgb( 0, 0, 50 )";
			this.fillRect( x, y, w, h );
			this.strokeStyle = "rgb( 90, 90, 90 )";
			this.strokeRect( x, y, w, h );
			this.strokeStyle = "rgb( 200,255,200 )";
			this.strokeRect( x + 1, y + 1, w - 2, h - 2 );
			this.strokeStyle = "rgb( 255,255,255 )";
			this.strokeRect( x + 2, y + 2, w - 4, h - 4);
			this.strokeStyle = "rgb( 90, 90, 90 )";
			this.strokeRect( x + 3, y + 3, w - 6, h - 6);
		}
	}

})();