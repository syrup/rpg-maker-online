function ressloader( source ) {
	var self = this;
	this.res = [];

	self.load = function( id, callback ) {
		if( !self.res[id] ) {
			ajax.json( source+id, function( data ) { self.init( id, data, callback ); });
		} else {
			callback( self.res[id] );
		}
	};

	this.init = function( id, data, callback ) {
		console.log( id, data, callback );
	};
}

var chipsets = new ressloader( 'load.php?chipset=' );
chipsets.init = function( id, data, callback ) {
	var result = new chipset( data, function() {
		chipsets.res[id] = result;
		callback( result );
	});
};

var maps = new ressloader( 'load.php?map=' );
maps.init = function( id, data, callback ) {
	var result = new map( data );
	maps.res[id] = result;
	callback( result );
};

