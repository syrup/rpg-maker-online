function selectable( options ) {
	this.setOptions(options);
}

selectable.prototype.setOptions = function( options ) {
	this.option = 0;
	this.changed = true;
	this.options = [];
	this.callbacks = [];

	for( var i in options ) {
		this.options.push( i );
		this.callbacks.push( options[i] );
	}
}

selectable.prototype.down = function( key ) {
	if( key == 'down' ) this.next();
	else if( key == 'up' ) this.prev();
	else if ( key == 'enter' || key == 'space' ) this.apply();
	this.changed = true;
}

selectable.prototype.apply = function() {
	this.callbacks[this.option]();
}

selectable.prototype.next = function() {
	if( ++this.option >= this.options.length )
		this.option = 0;
}

selectable.prototype.prev = function() {
	if( --this.option < 0 )
		this.option = this.options.length - 1;
}