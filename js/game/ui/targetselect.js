function targetselect( options, callback ) {
	options.sort( function( a, b ) {return a.position-b.position});
	var option = 0;

	this.draw = function( ctx ) {
		var pos = options[option].getPos();
		ctx.drawImage(
			graphic[config.system.skin],
			129, 96, 32, 32,
			pos.x-16, pos.y+16, 32, 32 );
	}

	this.down = function( key ) {
		if( key == 'enter' || key == 'space' ) {
			callback( options[option] );
		} else if( key == 'left' ) {
			if( --option < 0 ) option = options.length-1;
		} else if( key == 'right' ) {
			if( ++option > options.length-1 ) option = 0;
		}
	}
}