function spinner( options ) {
	this.setOptions( options );
}

spinner.prototype = new selectable( {} );
spinner.prototype.parent = selectable.prototype;

spinner.prototype.anitime = 0;

spinner.prototype.draw = function( ctx, x, y, w ) {
	this.changed = false;
	ctx.drawBox( x, y, w, 2 );

	ctx.font = config.system.font;
	ctx.textAlign = 'center';

	ctx.fillText(
		this.options[this.option],
		config.tile.width*(x+w/2),
		config.tile.height*(y+1.5)-9);


	// animated arrows can be pain in the ass
	var g = graphic[config.system.skin];
	var frames = Math.floor( this.anitime / 150 );
	var frame = Math.abs( frames % 4 - 1);

	var sx = 160+(frame%2)*16;
	var sy = 64+Math.floor(frame/2)*16;
	var dx = config.tile.width*(x+w/2)-8;
	var dy = config.tile.height*(y+1);

	ctx.save();
	ctx.translate( dx, dy-7 );
	ctx.rotate( Math.PI );
	ctx.drawImage( g, sx, sy, 16, 16, -16, 0, 16, 16 );
	ctx.restore();

	ctx.drawImage( g, sx, sy, 16, 16, dx, dy+7, 16, 16 );
}

spinner.prototype.update = function( delta ) {
	this.anitime = ( this.anitime + delta ) % 600;
}