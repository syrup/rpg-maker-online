function choice( options, w, h, align ) {
	this.setOptions( options );
	this.w = w;
	this.h = h ? h : this.options.length+1;
	this.align = align ? align : 'left';
}

choice.prototype = new selectable( {} );
choice.prototype.parent = selectable.prototype;

choice.prototype.draw = function( ctx, x, y ) {
	this.changed = false;

	var w = config.tile.width;
	var h = config.tile.height;

	ctx.font = config.system.font;
	ctx.textAlign = this.align;

	ctx.drawBox( x, y, this.w, this.h );

	for( var i = 0; i < this.options.length; i++ ) {
		if( i == this.option ) ctx.fillStyle = config.system.selected;
		else ctx.fillStyle = config.system.text;

		switch( this.align ) {
			case 'center': var left = x+this.w/2; break;
			case 'right': var left = x+this.w-.5; break;
			default: var left = x+.5;
		}

		ctx.fillText( this.options[i], left*w, h*(y+i+1.5)-8, w*this.w );
	}
}
