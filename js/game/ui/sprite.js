function sprite( g ) {
	var self = this;

	graphic.urls.push( g );
	graphic.load( function() {
		self.img = graphic[g];
	});
}

sprite.prototype.draw = function( ctx, x, y ) {
	if( this.img ) {
		ctx.drawImage( this.img, x, y );
	}
};

sprite.prototype.center = function( ctx, x, y ) {
	if( this.img ) {
		ctx.drawImage( this.img, x-this.img.width/2, y-this.img.height/2 );
	}
};

function characterSprite( g ) {
	var self = this;

	graphic.urls.push( g );
	graphic.load( function() {
		self.img = graphic[g];
		self.w = Math.floor( graphic[g].width/3 );
		self.h =  Math.floor( graphic[g].height/4 );
		self.x = Math.round(( self.w-config.tile.width ) / 2 );
		self.y = self.h-config.tile.height;
	});
}

characterSprite.prototype.draw = function( ctx, x, y, d, f ) {
	if( this.img ) {
		var dx = x - this.x;
		var dy = y - this.y;

		if( dx + this.w < 0 ) return;
		if( dx > screens.map.w ) return;
		if( dy + this.h < 0 ) return;
		if( dy > screens.map.h ) return;

		ctx.drawImage( this.img, f * this.w, d * this.h, this.w, this.h, dx, dy, this.w, this.h );
	}
};

function animationSprite( g, frames ) {
	var self = this;

	graphic.urls.push( g );
	graphic.load( function() {
		self.img = graphic[g];
		self.h = graphic[g].height;
		self.w = graphic[g].width / frames;
		self.f = frames;
	});
}

animationSprite.prototype.draw = function( ctx, x, y, f ) {
	if( this.img ) {
		ctx.drawImage( this.img, f*this.w, 0, this.w, this.h, x-this.w/2, y-this.h/2, this.w, this.h );
	}
};