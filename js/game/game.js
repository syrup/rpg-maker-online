window.requestAnimFrame = (function(){
	return window.requestAnimationFrame ||
		window.webkitRequestAnimationFrame ||
		window.mozRequestAnimationFrame ||
		window.oRequestAnimationFrame ||
		window.msRequestAnimationFrame ||
		function( callback, element ){window.setTimeout(callback, 25);};
})();

var game = {
	frames: 0,
	fps: 25,

	screen: null,
	lastUpdate: 0,

	display: null,
	displayCtx: null,
	buffer: null,
	bufferCtx: null,

	init: function() {
		var self = this;
		self.createBuffer();

		self.lastUpdate = Date.now();
		setInterval( function() { self.updateFramerate(); }, 1000 );
		self.loop();
	},

	updateFramerate: function() {
		this.fps = this.frames;
		this.frames = 0;
	},

	loop: function() {
		var now = Date.now();
		var delta = now - this.lastUpdate;

		if( delta < 250 && this.screen ) {
			this.update( delta );
			this.draw();
		}

		this.lastUpdate = now;
		this.frames++;

		requestAnimFrame( function() { game.loop(); });
	},

	update: function( delta ) {
		this.screen.update( delta );
	},

	draw: function() {
		this.screen.draw( this.bufferCtx );

		this.display.width = this.display.width;
		this.displayCtx.drawImage( this.buffer, 0, 0 );

		// fsps display
		this.displayCtx.fillStyle = 'white';
		this.displayCtx.font = '10px monospace';
		this.displayCtx.fillText( this.fps, 600, 460 );
	}
}