function screen() {}

screen.prototype.w = 640;
screen.prototype.h = 480;

screen.prototype.draw = function( ctx ) {}
screen.prototype.update = function( delta ) {}

screens = {
	init: function() {
		this.title = new titleScreen();
		this.map = new mapScreen();
		this.dialog = new dialogScreen();
		this.menu = new menuScreen();
		this.battle = new battleScreen();
	}
}