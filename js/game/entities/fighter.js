function fighter() {}

fighter.prototype = new entity();
fighter.prototype.parent = entity.prototype;


fighter.prototype.position = 0;
fighter.prototype.name = 'unknown';

fighter.prototype.ini = 0;
fighter.prototype.countdown = 0;

fighter.prototype.status = null;
fighter.prototype.life = 1;
fighter.prototype.mana = 1;

fighter.prototype.vitality = 1; // life
fighter.prototype.willpower = 1; // mana

fighter.prototype.agility = 1; // hit chance, avoind chance
fighter.prototype.strength = 1; // physical damage, hit chance, avoind chance
fighter.prototype.intelligence = 1; // magic damage

fighter.prototype.resistance = 0; // magic damage reduction
fighter.prototype.armor = 0; // physical damage reduction
fighter.prototype.damage = [ 10, 10 ];


fighter.prototype.setPos = function( i ) {
	this.position = i;
}

fighter.prototype.loadStats = function( data ) {
	for( var i in data )
		this[i] = data[i];

	this.life = this.getMaxLife();
	this.mana = this.getMaxMana();
}

fighter.prototype.getBaseIni = function() {
	return 100 / ( this.ini + 100 );
}

fighter.prototype.getMaxLife = function() {
	return this.vitality * 5;
}

fighter.prototype.getMaxMana = function() {
	return this.willpower * 5;
}