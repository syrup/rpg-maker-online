function mapentity() {}

mapentity.prototype = new entity();
mapentity.prototype.parent = entity.prototype;

mapentity.prototype.w = config.tile.width;
mapentity.prototype.h = config.tile.height;

mapentity.prototype.sprite = null;
mapentity.prototype.frame = 1;
mapentity.prototype.direction = 0;

// tile coordinates
mapentity.prototype.x = 0;
mapentity.prototype.y = 0;
// absolute coordinates
mapentity.prototype.absx = 0;
mapentity.prototype.absy = 0;

// if object is moving
mapentity.prototype.move = false;
// string, containing moving direction
mapentity.prototype.movedir = null;

// remaining frames for the animation
mapentity.prototype.aniframe = 0;
// movement vector for the animation
mapentity.prototype.anidir = {x: 0, y: 0, m: 0};
// duration of the animation
mapentity.prototype.anitime = 0;

// some constants for animations
mapentity.prototype.frameduration = 100;
mapentity.prototype.framecount = 4;

mapentity.prototype.setpos = function( x, y ) {
	this.x = x;
	this.y = y;
	this.absx = this.x * this.w;
	this.absy = this.y * this.h;
};

mapentity.prototype.load = function( sprite ) {
	this.sprite = new characterSprite( sprite );
}

mapentity.prototype.animate = function( delta ) {
	this.anitime += delta;

	var frames = Math.floor( this.anitime / this.frameduration );
	var progress = delta / (this.frameduration * this.framecount);

	this.aniframe = this.framecount - frames;
	this.frame = Math.abs( this.aniframe % 4 - 1);

	this.absx += this.anidir.x*this.w*progress;
	this.absy += this.anidir.y*this.h*progress;
}

mapentity.prototype.startmove = function( movedir, map ) {
	var dir = {x: 0, y: 0, m: 1};

	switch( movedir ) {
		case 'up': dir.y--;this.direction = 0;break;
		case 'down': dir.y++;this.direction = 2;break;
		case 'left': dir.x--;this.direction = 3;break;
		case 'right': dir.x++;this.direction = 1;break;
		default: return false;
	}

	// new position after movement
	var neu = {x: this.x+dir.x, y: this.y+dir.y};

	// initialize animation
	this.aniframe = this.framecount;
	this.anidir = {x: 0, y: 0, m: 0};
	this.anitime = 0;

	// hm a bit hacky :-/
	if( neu.x == game.screen.hero.x && neu.y == game.screen.hero.y )
		return false;

	if( !map.valid( neu.x, neu.y ))
		return false;

	if( !this.canPass( map.find( neu.x, neu.y )))
		return false;

	if( !map.free( neu.x, neu.y ))
		return false;

	this.anidir = dir;
	this.x += dir.x;
	this.y += dir.y;
	return true;
}

mapentity.prototype.update = function( delta, map ) {};
mapentity.prototype.canPass = function( event ) {};

mapentity.prototype.draw = function( ctx, viewport ) {
	this.sprite.draw( ctx, this.absx-viewport.x, this.absy-viewport.y, this.direction, this.frame );
};
