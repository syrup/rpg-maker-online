function hero( x, y ) {
	this.setpos(x, y);
	this.load('img/charset/lufia_dekar.gif');
}

hero.prototype = new mapentity();
hero.prototype.parent = mapentity.prototype;

hero.prototype.map = null;

hero.prototype.action = function() {
	if( this.anidir.m && this.aniframe ) return;

	if( this.map ) {
		var below = this.map.find( this.x, this.y );
		if( below && below.trigger == 2 )
			below.execute();

		var dir = {x: this.x, y: this.y };

		switch( this.direction ) {
			case 0: dir.y--;break;
			case 1: dir.x++; break;
			case 2: dir.y++; break;
			case 3: dir.x--; break;
		}

		var next = this.map.find( dir.x, dir.y );
		if( next && next.trigger == 2 && next.level == 1 )
			next.execute();
	}
}

hero.prototype.down = function( key ) {
	if( key == 'space' ) {
		this.action();
	} else {
		this.move = true;
		this.movedir = key;
	}
}

hero.prototype.up = function( key ) {
	if( key == this.movedir ) {
		this.move = false;
	}
}

hero.prototype.update = function( delta ) {
	if( this.aniframe > 0 ) {
		this.animate(delta);

		if( this.aniframe < 1 ) {
			// recalculate position to avoid rounding mistakes
			this.setpos( this.x, this.y );
			//check for event
			var event = this.map.find( this.x, this.y );

			if( this.anidir.m && event && event.trigger == 1 ) {
				this.move = false;
				event.execute();
			}
		}
	}

	// no else block, because aniframe can become 0 in the above block
	if( this.move && this.aniframe < 1 ) {
		this.startmove( this.movedir, this.map );
	}
};

hero.prototype.canPass = function( event ) {
	if( event && event.level == 1 ) {
		if( event.trigger == 1 ) {
			this.move = false;
			event.execute();
		}

		return false;
	} else {
		return true;
	}
};

/**


	var faktor = 4;
	var max = 100;

	self.attributes = {
		name: 'Dekar',
		clas: 'Fighter',
		status: 'normal',
		lvl: 0,
		str: 10,
		def: 10,
		inte: 5,
		agi: 15,
		mga: 5,
		hp:  30,
		maxHp: 30,
		mp: 1200,
		maxMp: 5130,
	};

	self.action = function() {
		if(( self.anidir.x != 0 || self.anidir.y != 0 ) && self.aniframe ) return false;
		var map = currentmap;

		if( map.events[self.y] && map.events[self.y][self.x] && map.events[self.y][self.x].trigger == 2 ) {
			map.events[self.y][self.x].execute();
		} else {
			var dir = {x: self.x, y: self.y };

			switch( hero.direction ) {
				case 0: dir.y--;	break;
				case 1: dir.x++; break;
				case 2: dir.y++; break;
				case 3: dir.x--; break;
			}

			if( map.events[dir.y] && map.events[dir.y][dir.x]  ) {
				var ev = map.events[dir.y][dir.x];
				if( ev.trigger == 2 && ev.level == 1 )
					ev.execute();
			}
		}
	};



*/