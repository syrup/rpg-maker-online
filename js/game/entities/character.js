function character( data, sprite ) {
	this.sprite = sprite;
	this.loadStats(data);
}

character.prototype = new fighter();
character.prototype.parent = fighter.prototype;

character.prototype.getPos = function() {
	var y = 13.5 * config.tile.height;
	var x = ( this.position*5 + .3 ) * config.tile.width;
	return { x: x + 16, y: y };
}

character.prototype.drawIcon = function( ctx, pos ) {
	if( this.icon ) {
		ctx.lineWidth = 2;
		ctx.fillStyle = config.system.background;
		ctx.fillRoundRect( pos, 5, 40, 40, 4 );
		ctx.drawImage( this.icon, pos, 5 );
		ctx.strokeRoundRect( pos, 5, 40, 40, 4 );
	} else if( this.sprite.img ) {
		this.icon = document.createElement('canvas');
		this.icon.width = this.icon.height = 40;
		this.sprite.draw( this.icon.getContext('2d'), 4, 30, 2, 1 );
	}
}

character.prototype.draw = function( ctx, icon ) {
	var w = config.tile.width;
	var h = config.tile.height

	var y = 13.5 * h;
	var x = ( this.position*5 + 1.5 ) * w;

	this.sprite.draw( ctx, (this.position*5+.3)*w, y, 2, 1);

	ctx.progressBar( this.life / this.getMaxLife(), x, y+ 4, 100, 10, 2, ['#600', '#B00'] );
	ctx.progressBar( this.mana / this.getMaxMana(), x, y+18, 100, 10, 2, ['#006', '#00b'] );
	ctx.fillStyle = config.system.text;

	ctx.textAlign = 'left';
	ctx.font = config.system.font;
	ctx.fillText( this.name, x, y-2 );
}