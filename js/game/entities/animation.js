function animationDamage( target, damage ){
	this.target = target;
	this.damage = String(damage).split('');
	this.total = damage;
	this.done = 0;
	this.duration = 700;
	this.anitime = 0;

	console.log( target.life );

	var pos = target.getPos();
	this.x = pos.x;
	this.y = pos.y;
}

animationDamage.prototype.update = function( delta ) {
	var prog = this.anitime / this.duration;
	var dmg = Math.max( 0, Math.round( this.total * prog ) - this.done );

	this.done += dmg;
	this.target.life = Math.max( 0, this.target.life - dmg );

	return ( this.anitime += delta ) < this.duration;
}

animationDamage.prototype.draw = function( ctx ){
	ctx.font = '14pt Verdana, sans-serif';
	ctx.textAlign = 'left';

	var x = this.x - (this.damage.length * 12 ) / 2;

	for (var i=0; i < this.damage.length; i++ ) {
		//var y = this.y + 10 * Math.cos( Math.PI*2*prog - Math.PI*i*.2 );
		var y = this.y + 15 * Math.cos( Math.PI*2*(this.anitime+50*i)/(this.duration+50*i));

		ctx.fillStyle = "rgba(30,30,30,0.5)";
		ctx.fillText(this.damage[i], x+12*i, y);
		ctx.fillStyle = "rgba(230,230,230,1)";
		ctx.fillText(this.damage[i], x+12*i-1, y-1 );
	}
}

function animationText( target, text ){
	this.target = target;
	this.text = text;
	this.duration = 700;
	this.anitime = 0;

	var pos = target.getPos();
	this.x = pos.x;
	this.y = pos.y;
}

animationText.prototype.update = function( delta ) {
	return ( this.anitime += delta ) < this.duration;
}

animationText.prototype.draw = function( ctx ){
	ctx.font = '14pt Verdana, sans-serif';
	ctx.textAlign = 'center';

	var x = this.x - ctx.measureText(this.text).width / 2;
	var y = this.y + 15 * Math.cos( Math.PI*2*(this.anitime+50)/(this.duration+50));

	ctx.fillStyle = "rgba(30,30,30,0.5)";
	ctx.fillText(this.text, x, y);
	ctx.fillStyle = "rgba(230,230,230,1)";
	ctx.fillText(this.text, x-1, y-1 );
}

function animationBattle( target , url, frames, duration ) {
	this.target = target;
	this.frames = frames;

	this.sprite = new animationSprite( url, frames );
	this.counter = new framecounter( duration );

	var pos = target.getPos();
	this.x = pos.x;
	this.y = pos.y;
}

animationBattle.prototype.update = function( delta ) {
	this.counter.update( delta );
	return this.counter.frame < this.frames;
}

animationBattle.prototype.draw = function( ctx ){
	ctx.globalAlpha = (this.frames - this.counter.frame) / this.frames
	ctx.globalCompositeOperation = 'destination-out';
	this.target.draw( ctx );
	ctx.globalAlpha = 1
	ctx.globalCompositeOperation = 'source-over';

	this.sprite.draw( ctx, this.x, this.y, this.counter.frame );
}