function monster( data, sprite ) {
	this.sprite = sprite;
	this.loadStats(data);
}

monster.prototype = new fighter();
monster.prototype.parent = fighter.prototype;

monster.prototype.getPos = function() {
	return { x: this.position, y: 224 };
}

monster.prototype.drawIcon = function( ctx, pos ) {
	ctx.lineWidth = 2;
	ctx.fillStyle = config.system.background;
	ctx.fillRoundRect( pos, 5, 40, 40, 4 );

	if( this.sprite.img ) {
		var x = this.sprite.img.width/2 - 30;
		var y = this.sprite.img.height/2 - 90;
		ctx.drawImage( this.sprite.img, x, y, 60, 60, pos, 5, 40, 40 );
	}

	ctx.strokeRoundRect( pos, 5, 40, 40, 4 );
}

monster.prototype.draw = function( ctx ) {
	this.sprite.center( ctx, this.position, 224 );
}

monster.prototype.turn = function() {
	return null;
}