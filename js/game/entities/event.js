function event( map, data ) {
	this.map = map;
	this.id = 'event'+data.id
	this.pages = data.pages;
	this.setpos( Number( data.x ), Number( data.y ));

}

event.prototype = new mapentity();
event.prototype.parent = mapentity.prototype;

event.prototype.map = null;
event.prototype.page = 0;
event.prototype.pages = [9];
event.prototype.direction = 2;

event.prototype.level = 0;
event.prototype.trigger = 0;

event.prototype.setPage = function( page ) {
	if( page < 0 || page >= this.pages.length )
		return;

	this.level = this.pages[page].level;
	this.trigger = this.pages[page].trigger;
	this.script = this.pages[page].script;
	this.movement = this.pages[page].movement;

	this.load( this.pages[page].sprite );
	this.page = vars[this.id] = page;
}

event.prototype.nextPage = function() {
	if( this.page + 1 < this.pages.length )
		this.setPage( this.page + 1 );
}

event.prototype.remove = function() {
	vars[this.id] = -1;
	delete this.map.events[this.y][this.x];
}

event.prototype.update = function( delta ) {
	if( self.trigger == 3 )
		self.execute();


	if( this.aniframe > 0 ) {
		this.animate(delta);

		if( this.aniframe < 1 ) {
			// recalculate position to avoid rounding mistakes
			this.setpos( this.x, this.y );
		}
	}

	// no else block, because aniframe can become 0 in the above block
	if( this.aniframe < 1 )
		if( this.move ) {
			this.move = false;
			this.remove();

			this.startmove( this.movedir, this.map );
			if( typeof this.map.events[this.y] == 'undefined' )
				this.map.events[this.y] = [];

			this.map.events[this.y][this.x] = this;
		} else if ( this.movement == 1 ) {
			var rnd = ( Math.random() * 4 ) | 0;
			var dirs = ['up', 'down', 'left', 'right'];

			this.move = true;
			this.movedir = dirs[rnd];
		}
};

event.prototype.canPass = function( event ) {
	if( event ) return false;
	else return true;
};





/**

	var faktor = 4;
	var charset = false;

	function message( ) {
		var lines =[];
		for(i in arguments){
			if( typeof arguments[i] == 'string' ) lines[i] = arguments[i];
		}
		dialog.show( lines );
	}

	function fade(type,delay,color,callback){
		if(!type){type="out";}
		if(!delay){delay=20;}
		if(!color){color=[0,0,0];}
		switch(type){
			case "in":
				new Fade(1,delay,color,callback);
			break;
			case "out":
				new Fade(0,delay,color,callback);
				break;
			case "outin":
				new Fade(0,delay,color,function(){new Fade(1,delay,color,callback);});
			break;
			case "inout":
				new Fade(1,delay,color,function(){new Fade(0,delay,color,callback);});
			break;
			default:
				alert( 'Unknown Parameter: ' + type);
		}
	}
	function animation( id ) {

	}

	function delay( frames, callback ) {
		new Delay( frames, callback );
	}

}
*/