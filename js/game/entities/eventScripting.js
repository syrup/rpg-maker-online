var eventBuffer = [];

(function() {
	// Functions for event scripting
	function message( lines ) {
		eventBuffer.push( function() {
			screens.dialog.message( lines );
			game.screen = screens.dialog;
			return true;
		});
	}

	function teleport( map, x, y ) {
		eventBuffer.push( function() {
			if( game.screen.teleport )
				game.screen.teleport( map, x, y );
			return true;
		});
	}

	function wait( sec ) {
		var buffer = 0;
		sec *= 1000;

		eventBuffer.push( function( delta ) {
			buffer += delta;
			return buffer >= sec;
		});
	}

	event.prototype.execute = function() {
		var self = this;

		function remove() { eventBuffer.push( function() { self.remove(); return true; }); }
		function next() { eventBuffer.push( function() { self.nextPage(); return true; }); }
		function page( id ) { eventBuffer.push( function() { self.setPage( id ); return true; }); }

		eval( this.script );
	}
})();
