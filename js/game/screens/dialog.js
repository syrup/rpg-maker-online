function dialogScreen() {
	var bg = createBox( 20, 5 );
	var changed = true;

	var lines = [];
	var selected = -1;


	this.message = function( message ) {
		changed = true;
		selected = -1;

		if( typeof message != 'string' ) lines = message;
		else lines = [ message ];
	}

	this.choice = function( options ) {
		changed = true;
		selected = 0;
		console.log( options );
	}

	this.draw = function( ctx ) {
		if( changed ) {
			changed = false;
			ctx.drawImage(bg,0,320);

			ctx.font = config.system.font;
			ctx.textAlign = 'left'

			var o = lines.length;
			var w = config.tile.width;
			var h = config.tile.height;

			for( var i = 0; i < lines.length; i++ ) {
				if( i == selected ) ctx.fillStyle = config.system.selected;
				else ctx.fillStyle = config.system.text;
				ctx.fillText( lines[i], w*.5, h*(i+11.5)-8, w*13 );
			}
		}
	}

	this.down = function( key ) {
		if ( key == 'enter' || key == 'space' )
			game.screen = screens.map;
	}
}

dialogScreen.prototype = new screen();
dialogScreen.prototype.parent = screen.prototype;

/*
var dialog = {
	init: false,
	offset: 0,
	lines: [],

	update: function() {

	},

	repaint: function() {
		if( this.init ) return false;
//		drawbox( 0, 0, 160, 224 );
//		drawbox( 160, 0, 480, 480 );
//		drawbox( 0, 224, 160, 64 );
//		drawbox( 10, 200, 608, 160 );
		drawbox( 0, 320, 640, 160 );
		screen.b.font = '20px monospace';
		screen.b.fillStyle = '#FFFFFF';
		for( var i = this.offset; i < this.lines.length && i < this.offset+4; i++ )
			screen.b.fillText( this.lines[i], 20, 355+i*30 );
		this.init = true;
	},

	down: function( key ) {
		if( key == 'space' ) {
			if( this.offset+4 < this.lines.length ) {
				this.init = false;
				this.offset += 4;
			} else {
				screen.current = currentmap;
			}
		}
	},

	up: function( key ) {
	},

	show: function( lines ) {
		this.init = false;
		this.offset = 0;
		this.lines = lines;
		screen.current = this;
	}
}*/