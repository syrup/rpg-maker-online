function menuScreen() {

	var menu = new choice( {
		'Ites': function() {},
		'Party': function() {},
		'Save': function() {},
		'Back': function() { game.screen = screens.map; }
	}, 5 );

	this.draw = function( ctx ) {
		if( menu.changed ) {
			menu.draw( ctx, .5, .5 );
			ctx.drawBox( .5, 6.5, 5, 8 );
			ctx.drawBox( 6.5, .5, 13, 14 );
		}
	}

	this.down = function( key ) {
		menu.down( key );
	}
}

menuScreen.prototype = new screen();
menuScreen.prototype.parent = screen.prototype;

/*
var menu = {
	init: false,
	offset: 0,
	lines: [[128,64,32,32],[160,64,16,16],[186,64,16,16],[160,80,16,16],[186,80,16,16]],
	menutiles: [[128,64,32,32],[160,64,16,16],[186,64,16,16],[160,80,16,16],[186,80,16,16]],
	submenu: ['Item','Skill','Equip','Status','Save','End Game'],
	highlight: 0,
	menutype : "",
	update: function() {

	},

	repaint: function() {
		if( this.init ) return false;
		var ctx = screen.b;
		switch(this.menutype){
			case "main":
			drawbox( 0, 0, 160, 224 );
			drawbox( 160, 0, 480, 480 );
			drawbox( 0, 224, 160, 64 );
			ctx.font = '20px monospace';
			ctx.fillStyle = '#FFFFFF';

			var g = graphic['menubox'];
			ctx.drawImage(g, this.menutiles[0][0], this.menutiles[0][1], this.menutiles[0][2], this.menutiles[0][3], 30, 18+this.highlight*30, 100, 24); //bg

			for( var i = 0; i < this.submenu.length; i++ ){
				ctx.fillText( this.submenu[i], 32, 36+i*30 );
			}
			ctx.textAlign = 'right';
			ctx.fillText( party.gold+' G', 140, 264);
			ctx.textAlign = 'left';

			var player = party.players;
			for(var i=0;i<player.length;i++) {	//zeichne soviele Spieler wie vorhanden
				ctx.drawImage(player[i].sprite[0], 55,130,32,64,184, 16+i*114,32*1.25,64*1.25);	//Abstand der player: y=114
				ctx.fillText( 'Name: '+player[i].attributes.name, 232, 32+i*114);
				ctx.fillText( 'Klasse: '+player[i].attributes.clas, 232+184, 32+i*114);
				ctx.fillText( 'Lv: '+player[i].attributes.lvl, 232, 32+30+i*114);
				ctx.fillText( 'HP:', 232+184, 32+30+i*114);
				ctx.fillText( 'MP:', 232+184, 32+60+i*114);
				ctx.fillText( 'Status: '+player[i].attributes.status, 232, 32+60+i*114);
				ctx.textAlign = 'right';
				ctx.fillText(player[i].attributes.hp+"/"+player[i].attributes.maxHp, 232+184+150, 32+30+i*114);
				ctx.fillText(player[i].attributes.mp+"/"+player[i].attributes.maxMp, 232+184+150, 32+60+i*114);
				ctx.textAlign = 'left';
				this.init = true;

			}
			break;
			case "item":
			drawbox( 0, 0, 160, 224 );
			drawbox( 160, 0, 480, 480 );
			fillText(ctx,"ich bin ein tolles item",170,200,'#00aa00');
			break;
		}
	},

	down: function( key ) {
		if( key == 'esc' ) {
			if( this.offset+4 < this.lines.length ) {
				this.init = false;
				this.offset += 4;
			} else {
				screen.current = currentmap;
			}
		}else if(key == 'down'){
			this.init = false;
			this.highlight = (this.highlight+1)%6;
		}
		else if(key == 'up'){
			this.init = false;
			this.highlight = Math.abs((this.submenu.length+this.highlight-1))%6;
		}
		else if(key == 'enter'){
			this.menutype = 'item';
			this.init = false;
		}
	},

	up: function( key ) {
	},

	show: function( lines ) {
		this.init = false;
		this.offset = 0;
		this.lines = lines;
		this.highlight =0;
		this.menutype = "main";
		screen.current = this;
	}

}
*/