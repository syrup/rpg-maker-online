function titleScreen() {
	var sprite = null;

	function newGame() {
		game.screen = screens.map;
		game.screen.load( config.start.map );

		screens.battle.init();
		game.screen = screens.battle;
	}

	var menu = new choice({
		'New Game': newGame,
		'Load Game': function() { console.log('gibs noch nict'); },
		'Exit': function() { window.close(); }
	}, 10, 0, 'center' );

	this.draw = function( ctx ) {
		if( sprite && menu.changed ) {
			ctx.drawImage(sprite,0,0);
			menu.draw( ctx, 5, 10 );
		}
	}

	this.down = function( key ) {
		menu.down( key );
	}

	graphic.urls.push( config.system.title );
	graphic.load( function() {
		sprite = graphic[config.system.title];
	});
}

titleScreen.prototype = new screen();
titleScreen.prototype.parent = screen.prototype;
