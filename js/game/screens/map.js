function mapScreen() {
	var self = this;

	var buffer = {
		upper: document.createElement('canvas'),
		lower: document.createElement('canvas')
	};

	buffer.lower.width = buffer.upper.width = this.w;
	buffer.lower.height = buffer.upper.height = this.h;

	buffer.u = buffer.upper.getContext('2d');
	buffer.l = buffer.lower.getContext('2d');

	var viewport = {
		x: 0,
		y: 0,
		w: self.w,
		h: self.h,

		set: function( x, y ) {
			if( x < 0 ) x = 0;
			if( y < 0 ) y = 0;

			if( x > buffer.lower.width - self.w )
				x = buffer.lower.width - self.w;
			if( y > buffer.lower.height - self.h )
				y = buffer.lower.height - self.h;

			this.x = x;
			this.y = y;
		},

		visible: function( x, y, w, h ) {
			if( x < this.x - w ) return false;
			if( x > this.x + this.w ) return false;

			if( y < this.y - h ) return false;
			if( y > this.y + this.h ) return false;

			return true;
		}
	}

	self.map = null;
	self.hero = new hero( config.start.x, config.start.y );

	self.load = function( id ) {
		maps.load( id, function( map ) {
			map.drawbuffer( buffer, function() {
				self.hero.map = self.map = map;
			});
		});
	}

	self.draw = function( ctx ) {
		if( self.map ) {
			var events = self.map.events;
			ctx.drawImage( buffer.lower, viewport.x, viewport.y, this.w, this.h, 0, 0, this.w, this.h );

			for( y = 0; y < self.map.height; y++ ) {
				if( events[y] )
					for( x in events[y] )
						events[y][x].draw( ctx, viewport );

				if( y == self.hero.y )
					self.hero.draw( ctx, viewport );
			}

			ctx.drawImage( buffer.upper, viewport.x, viewport.y, this.w, this.h, 0, 0, this.w, this.h );
		}
	}

	self.update = function( delta ) {
		if( self.map ) {
			if( eventBuffer.length ) {
				if( eventBuffer[0]( delta ) )
					eventBuffer.shift();
				return;
			}

			var events = self.map.events;
			self.hero.update( delta );

			for( y in events )
				for( x in events[y] )
					events[y][x].update( delta );

			viewport.set( self.hero.absx - self.w / 2, self.hero.absy - self.h / 2 );
		}
	}

	self.down = function( key ) {
		if( key == 'esc' ) game.screen = screens.menu;
		else self.hero.down( key );
	};

	self.up = function( key ) {
		self.hero.up( key );
	};

	self.teleport = function( map, x, y ) {
		this.map = null;
		this.load( map );

		this.hero.map = null;
		this.hero.aniframe = 0;
		this.hero.setpos( x, y );
	}
}

mapScreen.prototype = new screen();
mapScreen.prototype.parent = screen;