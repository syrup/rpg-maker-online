function battleScreen() {
	var bg, bgurl = 'img/background/battle.png';
	var fighters = [], party = [], dead = [];

	var w = config.tile.width;
	var h = config.tile.height

	var offset, current = null;
	var phase = null;

	function filter( type ) {
		var result = [];

		for( var i = 0; i < fighters.length; i++ )
			if( fighters[i] instanceof type )
				result.push( fighters[i] );

		return result;
	}

	function endRound() {
		phase = null;

		//for( var i = 0; i < fighters.length; i++ )
		//	if( fighters[i].life <= 0 )
		//		phase = animationDie( fighters[i] );


	}

	function attack( target ) {
		var chance = .9 * current.agility / target.agility;

		if( Math.random() < chance ) {
			var modificator = ( 100 + current.strength ) * ( 100 - target.armor ) / 10000;
			var damage = Math.round(( current.damage[0] + Math.random() * current.damage[1] ) * modificator );

			phase = new aniStack([
				new animationBattle( target , 'img/animation/strike1.png', 5, 60 ),
				new animationDamage( target, damage )
			], endRound );
		} else {
			phase = new aniStack([ new animationText( target, 'miss' )], endRound );
		}
	}

	var menu = new choice( {
		'Attack': function() {phase = new targetselect( filter( monster ), attack );},
		'Spell': function() {phase = null;},
		'Item': function() {phase = null;},
		'Defend': function() {phase = null;}
	}, 5, 7 );

	var playerinput = {
		draw: function( ctx ) {
			menu.draw( ctx, current.position*5, 8 );
			current.draw( ctx );
		}, down: function( key ) {
			menu.down( key );
		}
	};

	this.prepare = function( enemies ) {
		offset = 2-party.length*.5
		fighters = [];

		for( var i = 0; i < enemies.length; i++ ) {
			fighters.push( enemies[i] );
			enemies[i].setPos((i+1)*(this.w/(enemies.length+1)));
			enemies[i].countdown = enemies[i].getBaseIni();
		}

		for( var i = 0; i < party.length; i++ ) {
			fighters.push( party[i] );
			party[i].setPos(i+offset);
			party[i].countdown = party[i].getBaseIni();
		}
	}

	this.init = function() {
		party = [
			new character( {name: 'Mittel',  ini: 4, vitality: 11}, new characterSprite('img/charset/znex.png')),
			new character( {name: 'Dekar',   ini: 3, vitality: 12}, screens.map.hero.sprite ),
			new character( {name: 'Schnell', ini: 5, vitality:  9}, screens.map.hero.sprite ),
			new character( {name: 'Dekar',   ini: 3, vitality: 10}, screens.map.hero.sprite ),
		];

		this.prepare([
			new monster({name: 'Zombie',   ini: 2, vitality: 12}, new sprite('img/battler/zombie.png')),
			new monster({name: 'Skeleton', ini: 3, vitality: 10}, new sprite('img/battler/skeleton.png')),
		]);
	}

	this.update = function( delta ) {
		if( phase ) {
			if( phase.update ) phase.update( delta );
		} else {
			fighters.sort( function( a, b ) {return a.countdown-b.countdown});
			current = fighters[0];
			var reduce = current.countdown;

			for( var i = 0; i < fighters.length; i++ )
				fighters[i].countdown -= reduce;
			current.countdown = current.getBaseIni();

			if( current instanceof character ) phase = playerinput;
			else attack( filter( character )[0] );
		}
	}

	this.draw = function( ctx ) {
		if( bg ) {
			ctx.drawImage( bg, 0, 0 );
			ctx.strokeStyle = config.system.selected;
			var TimelineOffset = ( this.w - fighters.length * 45 ) / 2;

			for( var i = 0; i < fighters.length; i++ ) {
				fighters[i].drawIcon( ctx, TimelineOffset + i*45 )
				fighters[i].draw( ctx );
				ctx.strokeStyle = config.system.text;
			}

			if( phase && phase.draw )
				phase.draw( ctx );
		}
	}

	this.down = function( key ) {
		if( phase && phase.down )
			phase.down( key );
	}

	graphic.urls.push( bgurl );
	graphic.load(function() {
		bg = graphic[bgurl];
	});
}

battleScreen.prototype = new screen();
battleScreen.prototype.parent = screen.prototype;
