
var brush = {x: 0, y: 0, w: 1, h: 1, activ: false, type: 'pen'};

var mapeditor = {
	w: 0,
	h: 0,

	data: null,
	layer: 0,

	canvas: null,
	chipset: null,

	alpha: true,
	raster: false,

	active: null,
	events: [],

	setlayer: function(  l ) {
		mapeditor.alpha = l != 'event';
		mapeditor.layer = l;
		mapeditor.draw();
		if( l == 'event' ) mapeditor.loadevents()
	},

	loadevents: function() {
		ajax.json( 'load.php?events='+mapeditor.id, function( data ) {
			mapeditor.draw();
			mapeditor.events = data;

			var tw = config.tile.width;
			var th = config.tile.height;

			var ctx = mapeditor.canvas.getContext("2d");
			ctx.globalAlpha = 0.5;

			for( var x = 0; x < mapeditor.w; x++ )
				for( var y = 0; y < mapeditor.h; y++ )
					if( data[x] && data[x][y] )
						ctx.drawImage( graphic.event, 0, 0, tw, th, x*tw, y*th, tw, th );
		});
	},

	toggleraster: function() {
		mapeditor.raster = !mapeditor.raster;
		mapeditor.draw();
	},

	tool: function( to ) {
		brush.type = to;
	},

	init: function() {
		var self = this;
		if( this.data.length < 1 ) this.data.push( [] );

		for( var l = 0; l < this.data.length; l++ ) {
			this.layerbutton(l);

			for( var x = 0; x < this.w; x++ ) {
				if( this.data[l][x] == undefined || this.data[l][x] == null ) this.data[l][x]= [];
			}
		}
	},

	layerbutton: function( l ) {
			var link = $( '<a href="javascript: mapeditor.setlayer('+l+');" id="layer'+l+'" class="btn">'+(l+1)+'</a>' );

			if( l == this.layer ) {
				link.addClass( 'active' );
			}

			$( '#addlayer' ).before( link );
	},

	addlayer: function() {
		var l = this.data.length;
		this.data[l] = [];

		for( var x = 0; x < this.w; x++ ) {
			this.data[l][x]= [];
		}

		this.layerbutton(l);
		$( '#layer'+this.layer ).button( 'toggle' );
	},

	removelayer: function() {
		if( this.data.length > 1 ) {
			$( '#layer'+(this.data.length-1)).remove();
			this.data.splice( this.layer, 1 );

			this.layer = 0;
			$( '#layer'+this.layer ).button( 'toggle' );
			mapeditor.draw();
		}
	},

	draw: function() {
		var tw = config.tile.width;
		var th = config.tile.height;

		this.canvas.width = this.w*tw;
		this.canvas.height = this.h*th
		var ctx = this.canvas.getContext("2d");

		for( var l = 0; l < this.data.length; l++ ) {
			ctx.globalAlpha = ( this.layer == l || !this.alpha ) ? 1.0 : 0.4;
			for( var x = 0; x < this.w; x++ )
				for( var y = 0; y < this.h; y++ )
					if( typeof this.data[l][x][y] != undefined && this.data[l][x][y] != null ) {
						this.chipset.tile( ctx, this.data[l][x][y], x, y, this.data[l] );
					}
		}

		if( this.raster )
			for( var x = 0; x < this.w; x++ )
				for( var y = 0; y < this.h; y++ ) {
					ctx.globalAlpha = 1.0;
					ctx.strokeStyle = "rgb( 255,255,255 )";
					ctx.strokeRect( x * tw, y * th, tw, th );
				}

	},

	start: function() {
		if( brush.type == "pen" && mapeditor.layer != 'event' )
			mapeditor.pen( Math.floor( mouse.x / 32), Math.floor( mouse.y / 32) );
		brush.activ = true;
	},

	drag: function() {
		var x = Math.floor(mouse.x/32);
		var y = Math.floor(mouse.y/32);

		if( brush.activ ) {
			if( brush.type == "pen" && mapeditor.layer != 'event' ) mapeditor.pen( x, y );
		}

		document.getElementById('coords').innerHTML = x+' - '+y;
	},

	stop: function() {
		if( brush.activ ) {
			var x = Math.floor(mouse.x/32);
			var y = Math.floor(mouse.y/32);

			if( mapeditor.layer == 'event' ) {
				//var win = window.open( 'editor.php?nomenu&site=event&map='+mapeditor.id+'&x='+x+'&y='+y, 'Eventeditor', 'width=800,height=1000,status=no,toolbar=no' );
				$( '#event_frame' ).attr( 'src', 'editor.php?nomenu&site=event&map='+mapeditor.id+'&x='+x+'&y='+y );
				$( '#event_editor' ).modal();
			}if( brush.type == "fill" ) {
				var chipid = selection.y * 8 + selection.x;
				mapeditor.fill( x, y, mapeditor.data[mapeditor.layer][x][y], chipid, true );
			}

			brush.activ = false;
		}
	},

	pen: function( set_x, set_y ) {
		for( x = 0; x < selection.w; x++ )
			for( y = 0; y < selection.h; y++ )
				if( set_x+x < this.w && set_y+y < this.h ) {
					var chipid = (selection.y + y) * 8 + (selection.x + x );
					this.data[this.layer][set_x+x][set_y+y] = chipid;
					this.redraw( set_x+x, set_y+y );
				}
	},

	fill: function( set_x, set_y, from, to, redraw ) {
		if( from != to ) {
			this.data[this.layer][set_x][set_y]= to;

			if( set_x+1 < this.w && this.data[this.layer][set_x+1][set_y] == from )
				this.fill( set_x+1, set_y, from, to, false );
			if( set_y+1 < this.h && this.data[this.layer][set_x][set_y+1] == from )
				this.fill( set_x, set_y+1, from, to, false );
			if( set_x-1 >= 0 && this.data[this.layer][set_x-1][set_y] == from )
				this.fill( set_x-1, set_y, from, to, false );
			if( set_y-1 >= 0 && this.data[this.layer][set_x][set_y-1] == from )
				this.fill( set_x, set_y-1, from, to, false );

			if( redraw ) this.draw( false );
		}
	},

	redraw: function( x, y ) {
		if( x-1 >= 0 ) {
			if( y-1 >= 0 ) this.chip( x-1, y-1 );
			this.chip( x-1, y );
			if( y+1 < this.h ) this.chip( x-1, y+1 );
		}

		if( y-1 >= 0 ) this.chip( x, y-1 );
		this.chip( x, y );
		if( y+1 < this.h ) this.chip( x, y+1 );

		if( x+1 < this.w ) {
			if( y-1 >= 0 ) this.chip( x+1, y-1 );
			this.chip( x+1, y );
			if( y+1 < this.h ) this.chip( x+1, y+1 );
		}
	},

	chip: function( x, y ) {
		var ctx = this.canvas.getContext("2d");

		var w = config.tile.width;
		var h = config.tile.height;
		ctx.clearRect( x * w, y * h, w, h );

		for( var l = 0; l < this.data.length; l++ ) {
			ctx.globalAlpha = ( this.layer == l || !this.alpha ) ? 1.0 : 0.4;
			if( typeof this.data[l][x][y] != undefined && this.data[l][x][y] != null ) {
				this.chipset.tile( ctx, this.data[l][x][y] , x, y, this.data[l] );
			}
		}
	},

	save: function( form ) {
		form['data'].value = mapeditor.data.toJSONString();
	}
};

var palette = {
	chipset: null,

	draw: function() {
		this.chipset.display( this.canvas );
		var ctx = this.canvas.getContext("2d");

		ctx.strokeStyle = "rgb( 0,0,0 )";
		ctx.strokeRect( selection.x * 32, selection.y * 32, selection.w * 32, selection.h * 32 );
		ctx.strokeStyle = "rgb( 200,255,200 )";
		ctx.strokeRect( selection.x * 32 + 1, selection.y * 32 + 1, selection.w * 32 - 2, selection.h * 32 - 2 );
		ctx.strokeStyle = "rgb( 255,255,255 )";
		ctx.strokeRect( selection.x * 32 + 2, selection.y * 32 + 2, selection.w * 32 - 4, selection.h * 32 - 4);
		ctx.strokeStyle = "rgb( 0,0,0 )";
		ctx.strokeRect( selection.x * 32 + 3, selection.y * 32 + 3, selection.w * 32 - 6, selection.h * 32 - 6);
	}
}

var anker = {x: 0, y: 0};

var selection = {
	x: 0,
	y: 0,
	w: 1,
	h: 1,

	activ: false,

	start: function() {
		selection.activ = true;
		anker.x = selection.x = Math.floor( mouse.x / 32 );
		anker.y = selection.y = Math.floor( mouse.y / 32 );
		selection.h = 1;
		selection.w = 1;
		palette.draw();
	},

	drag: function() {
		if( selection.activ ) {
			var x = Math.floor( mouse.x / 32 );
			var y = Math.floor( mouse.y / 32 );

			if( x > anker.x ) {
				selection.x = anker.x;
				selection.w = x - anker.x;
			} else {
				selection.x = x;
				selection.w = anker.x - x;
			}

			if( y > anker.y ) {
				selection.y = anker.y;
				selection.h = y - anker.y;
			} else {
				selection.y = y;
				selection.h = anker.y - y;
			}

			selection.w += 1;
			selection.h += 1;
			palette.draw();
		}
	},

	stop: function() {
		if( selection.activ ) {
			selection.activ = false;
		}
	}
};