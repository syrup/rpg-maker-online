var graphic = {
	afterload: null,

	urls: [],

	load: function( after ) {
		if( this.urls.length ) {
			var loading = this.urls.pop();
			var self = this;

			if( typeof loading == 'string' )
				loading = [ loading, loading ];

			if( typeof this[loading[0]] == 'undefined' ) {
				this[loading[0]] = new Image();
				this[loading[0]].onload = function() { self.load( after ) };
				this[loading[0]].src = loading[1];
			} else {
				self.load( after );
			}
		} else {
			after();
		}
	}
};