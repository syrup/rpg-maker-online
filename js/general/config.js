var config = {
	tile: {
		width: 32,
		height: 32
	},

	start: {
		map: 14,
		x: 15,
		y: 10
	},

	system: {
		title: 'img/background/title.png',
		skin: 'img/menu/default.png',
		text: 'white',
		background: 'rgba( 0,0,0, 0.5 )',
		selected: 'orange',
		font: '24px monospace'
	}
}