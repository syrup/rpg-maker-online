<!DOCTYPE html>
<html>
	<head>
		<title>RPG Maker Online</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<link rel="stylesheet" type="text/css" href="css/game.css">

		<script type="text/javascript" src="js/general/config.js"></script>
		<script type="text/javascript" src="js/general/graphic.js"></script>
		<script type="text/javascript" src="js/general/chipset.js"></script>
		<script type="text/javascript" src="js/general/ajax.js"></script>
		<script type="text/javascript" src="js/general/sound.js"></script>

		<script type="text/javascript" src="js/game/game.js"></script>
		<script type="text/javascript" src="js/game/buffer.js"></script>
		<script type="text/javascript" src="js/game/controls.js"></script>
		<script type="text/javascript" src="js/game/entity.js"></script>
		<script type="text/javascript" src="js/game/screen.js"></script>
		<script type="text/javascript" src="js/game/map.js"></script>
		<script type="text/javascript" src="js/game/ressources.js"></script>
		<script type="text/javascript" src="js/game/util.js"></script>

		<script type="text/javascript" src="js/game/ui/selectable.js"></script>
		<script type="text/javascript" src="js/game/ui/sprite.js"></script>
		<script type="text/javascript" src="js/game/ui/choice.js"></script>
		<script type="text/javascript" src="js/game/ui/spinner.js"></script>
		<script type="text/javascript" src="js/game/ui/battle.js"></script>

		<script type="text/javascript" src="js/game/screens/title.js"></script>
		<script type="text/javascript" src="js/game/screens/map.js"></script>
		<script type="text/javascript" src="js/game/screens/dialog.js"></script>
		<script type="text/javascript" src="js/game/screens/menu.js"></script>
		<script type="text/javascript" src="js/game/screens/battle.js"></script>

		<script type="text/javascript" src="js/game/entities/mapentity.js"></script>
		<script type="text/javascript" src="js/game/entities/hero.js"></script>
		<script type="text/javascript" src="js/game/entities/event.js"></script>
		<script type="text/javascript" src="js/game/entities/eventScripting.js"></script>
		<script type="text/javascript" src="js/game/entities/fighter.js"></script>
		<script type="text/javascript" src="js/game/entities/character.js"></script>
		<script type="text/javascript" src="js/game/entities/monster.js"></script>
		<script type="text/javascript" src="js/game/entities/animation.js"></script>

		<script type="text/javascript" src="data/global.js"></script>

		<script type="text/javascript">
			var vars = {};

			function init() {
				graphic.urls.push(config.system.skin);
				graphic.load( function() {
					screens.init();
					controls.init();
					game.init();

					game.screen = screens.title;
				});
			}
		</script>
	</head>

	<body onload="init();">
		<canvas id="gameframe" width="640" height="480"></canvas>
	</body>
</html>