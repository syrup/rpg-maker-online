<?php

require 'php/common.php';

if( !empty( $_GET['chipset'] )) {
	if( $chipset = $db->id_get( 'chipset', $_GET['chipset'] )) {
		$chipset['data'] = json_decode( $chipset['data'] );
		$chipset['autotile'] = $db->query( "SELECT autotile, level FROM autotile WHERE chipset = %d", $chipset['id'] )->rows();
		echo json_encode($chipset);
	}
} elseif( !empty( $_GET['map'] )) {
	if( $map = $db->id_get( 'map', $_GET['map'] )) {
		$map['data'] = json_decode( $map['data'] );

		foreach( $db->get( 'event', 'map = '.$map['id'] ) as $event ) {
			$event['pages'] = $db->get( 'event_page', 'event = '.$event['id'] );
			$map['events'][$event['y']][$event['x']] = $event;
		}

		echo json_encode($map);
	}
} elseif( !empty( $_GET['events'] )) {
	$events = array();

	foreach( $db->get( 'event', 'map = '.intval( $_GET['events'] )) as $e )
		$events[$e['x']][$e['y']] = 1;

	echo json_encode($events);
}
