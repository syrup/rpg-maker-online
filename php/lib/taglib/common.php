<?php

class taglib_common extends taglib_abstract {
	/**
		* Validiert ein Tag
		* @param array $tag Tag
		* @param array $args Attribute
		* @return boolean
		*/
	public function doValidate( $tag, $args ) {
		switch( $tag['open']['name'] ) {
			case 'out':       return $this->validateTag( $tag, $args, true,  array( 'var' ));
			case 'escape':    return $this->validateTag( $tag, $args, true,  array( 'var' ));
			case 'for':       return $this->validateTag( $tag, $args, false, array( 'var', 'from', 'to' ));
			case 'foreach':   return $this->validateTag( $tag, $args, false, array( 'var', 'value' ));
			case 'isempty':   return $this->validateTag( $tag, $args, false, array( 'var' ));
			case 'notempty':  return $this->validateTag( $tag, $args, false, array( 'var' ));
			case 'is':        return $this->validateTag( $tag, $args, false, array( 'var', 'value' ));
			case 'isnot':     return $this->validateTag( $tag, $args, false, array( 'var', 'value' ));
			case 'taglib':    return $this->validateTag( $tag, $args, true,  array( 'name', 'source' ));
			case 'inc':       return $this->validateTag( $tag, $args, true,  array( 'file' ));
			default:          return false;
		}
	}

	public function doTag( $tag, $args ) {
		if( $this->doValidate( $tag, $args )) {
			switch( $tag['open']['name'] ) {
				case 'out':
					return $this->expressionContext( $args['var'] );
					break;
				case 'escape':
					return htmlspecialchars( $this->expressionContext( $args['var'] ));
					break;
				case 'for':
					$from = $this->template->getExpression( $args['from'] );
					$to = $this->template->getExpression( $args['to'] );
					for( $i = $from; $i <= $to; $i++ ) {
						$this->template->setContext( $args['var'], $i );
						$result .= $this->parseBody( $tag );
					}
					return $result;
					break;
				case 'foreach':
					$arr = $this->expressionContext( $args['var'] );
					if( is_array( $arr ))
						foreach( $arr as $k => $i ) {
							if( !empty( $args['key'] ))
							$this->template->setContext( $args['key'], $k );
							$this->template->setContext( $args['value'], $i );
							$result .= $this->parseBody( $tag );
						}
					return $result;
					break;
				case 'isempty':
					$var = $this->expressionContext( $args['var'] );
					if( empty( $var )) return $this->parseBody( $tag );
					break;
				case 'notempty':
					$var = $this->expressionContext( $args['var'] );
					if( !empty( $var )) return $this->parseBody( $tag );
					break;
				case 'is':
					$var = $this->expressionContext( $args['var'] );
					$value = $this->template->getExpression( $args['value'] );
					if( $var == $value ) return $this->parseBody( $tag );
					break;
				case 'isnot':
					$var = $this->expressionContext( $args['var'] );
					$value = $this->template->getExpression( $args['value'] );
					if( $var != $value ) return $this->parseBody( $tag );
					break;
				case 'taglib':
					$name = $this->template->getExpression( $args['name'] );
					$klasse = $this->template->getExpression( $args['source'] );
					$this->template->addLib( $name, new $klasse());
					break;
				case 'inc':
					$file = $this->template->getExpression( $args['file'] );
					$inc = $this->template->loader->get( $file );
					$this->template->copyContext( $inc );
					$this->template->copyLibs( $inc );
					return $inc->parse();
					break;
				default:
					return false;
			} return "";
		} else return false;
	}
}
