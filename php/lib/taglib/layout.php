<?php

/*
 * Black Magic, do not touch :D
 * Extended lib to create more complex layouts
 * Work in progress
 */
class taglib_layout extends taglib_abstract {
	protected $menutag;
	protected $stack = array();

	public $js = array();
	public $css = array();

	/**
		* Validiert ein Tag
		* @param array $tag Tag
		* @param array $args Attribute
		* @return boolean
		*/
	public function doValidate( $tag, $args ) {
		switch( $tag['open']['name'] ) {
			case 'js': return $this->validateTag( $tag, $args, true );
			case 'css': return $this->validateTag( $tag, $args, true );
			case 'menu': return $this->validateTag( $tag, $args, false );
			case 'sub': return $this->validateTag( $tag, $args, true );
			default: return false;
		}
	}

	public function doTag( $tag, $args ) {
		if( $this->doValidate( $tag, $args )) {
			switch( $tag['open']['name'] ) {
				case 'js':
					$result = '';
					foreach( $this->js as $script )
						$result .= '<script type="text/javascript" language="javascript" src="'.$script.'"></script>';
					return $result;
				case 'css':
					$result = '';
					foreach( $this->css as $style )
						$result .= '<link rel="stylesheet" type="text/css" href="'.$style.'">';
					return $result;
				case 'menu':
					$this->menutag = $tag;
					if( empty( $args['var'] )) $args['var'] = 'menu';
				case 'sub':
					if( empty( $args['var'] )) $args['var'] = 'point';
					$point = $this->template->getContext( $args['var'] );
					$items = $point->children;

					if( $items ) {
						$this->template->setContext( 'points', $items );
						array_push( $this->stack, $point );
						$result = $this->parseBody( $this->menutag );
						$this->template->setContext( 'point', array_pop( $this->stack ));
						return $result;
					} else {
						return '';
					}
				default:
					return false;
			} return "";
		} else return false;
	}

	public function add_js( $file ) {
		$this->js[] = $file;
	}
	public function add_css( $file ) {
		$this->css[] = $file;
	}
}
