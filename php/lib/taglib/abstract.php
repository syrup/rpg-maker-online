<?php

abstract class taglib_abstract {
	protected $template;

	/**
		* Dependency Injection für das Template in der die Lib verwendet wird
		* @param iv_template_taglib_template $t Template
		*/
	public function setTemplate( taglib_template $t ) {
		$this->template = $t;
	}

	/**
		* Validiert ein Tag
		* @param array $tag Tag
		* @param array $args Gefundene Attribute
		* @param boolean $standalone Darf ein Tag als Standalone genutzt werden
		* @param array $required Benötigte Attribute
		* @return boolean
		*/
	protected function validateTag( $tag, $args, $standalone = false, $required = array()) {
		if( !isset( $tag['close'] ) && !$standalone ) return false;
		foreach( $required as $a ) if( empty( $args[$a] )) return false;
		return true;
	}

	/**
		* Gibt den uninterpretierten Inhalt eines Tags zutück
		* @param array $tag Tag
		* @return string uninterpretierter Inhalt
		*/
	protected function getBody( $tag ) {
		$start = $tag['open']['position'] + strlen( $tag['open']['source'] );
		if( isset( $tag['close'] ))
			return substr( $this->template->source, $start, $tag['close']['position'] - $start );
		else return "";
	}

	/**
		* Gibt den interpretierten Inhalt eines Tags zutück
		* @param array $tag Tag
		* @return string interpretierter Inhalt
		*/
	protected function parseBody( $tag ) {
		$start = $tag['open']['position'] + strlen( $tag['open']['source'] );
		if( isset( $tag['close'] ))
			return $this->template->parse_area( $start, $tag['close']['position'], $tag['tags'] );
		else return "";
	}

	/**
		* Interpretiert Variablen im Argument und gibt den entsprechenden Contextinhalt wieder
		* @param string $var Ausdruck
		* @return mixed Contextinhalt
		*/
	protected function expressionContext( $var ) {
		return $this->template->getContext($this->template->getExpression( $var ));
	}

	/**
		* Interpretiert ein das angegebene Tag
		* @param array $tag Tag
		* @param array $args Attribute
		* @return string Interpretiertes Tag
		*/
	abstract public function doTag( $tag, $args );
}
