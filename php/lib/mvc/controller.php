<?php

abstract class mvc_controller {
	protected $view;

	public function action_index() {
		return 'please overwrite index action';
	}

	public function __construct( mvc_view $view ) {
		$this->view = $view;
	}
}