<?php

interface mvc_view {
	public function content( $data );
	public function display();
}