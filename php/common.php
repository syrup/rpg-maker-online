<?php

error_reporting(E_ALL ^ E_NOTICE);

ini_set( 'display_errors', 1 );
ini_set( 'register_globals', 'off' );

require 'php/config.php';

function __autoload( $class ) {
	require 'php/lib/'.str_replace( '_', '/', $class ).'.php';
}

$db = new mysql_connection( $sql_host, $sql_user, $sql_pass, $sql_db );

// Magic Quotes entfernen
if( get_magic_quotes_gpc()) {
	function strip_quotes(&$value) { $value = stripslashes($value); }
	$gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
	array_walk_recursive($gpc, 'strip_quotes');
}

function error( $msg ) {
	echo '<div class="alert alert-error">'.$msg.'</div>';
}

function success( $msg ) {
	echo '<div class="alert alert-success">'.$msg.'</div>';
}

function db() {
	global $db;
	return $db;
}