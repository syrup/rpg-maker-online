<?php

$chipsets = $db->select( 'chipset' )->relate();


//$music = glob( "sound/music" );
//$music = array_combine( $music, $music );
$music[''] = 'Keine Musik';

$_POST['width'] = max( $_POST['width'], 20);
$_POST['height'] = max( $_POST['height'], 15);

$rc = new data_controller( 'map', SELF );
$rc->add( 'id', 'Id', 1, 0, 0 );
$rc->add( 'name', 'Name');
$rc->add( 'width', 'Breite' );
$rc->add( 'height', 'Höhe' );
$rc->add( 'chipset', 'Chipset', 1, 1, 1, 1, 'select', $chipsets );
$rc->add( 'music', 'Hintergrundmusik', 1, 1, 1, 0, 'select', $music );
$rc->add( 'data', 'Daten', 0, 0, 1, 0, 'hidden' );

if( $rc->run()) success( 'Änderungen erfolgreich gespeichert!' );

if( empty( $_GET['edit'] )) {
	echo '<table width="100%"><tr><td width="450">';
	echo '<div class="well"><h1>Karte erstellen</h1>';
	echo $rc->get_create().'</div></td><td>';
	echo '<div class="well"><h1>Karten verwalten</h1>';
	echo $rc->get_list().'</div></td></tr></table>';
} else {
	$map = $db->id_get( 'map', $_GET['edit'] );
	$chipset = $db->id_get( 'chipset', $map['chipset'] );
	$chipset['data'] = json_decode( $chipset['data'] );
	$chipset['autotile'] = $db->query( "SELECT autotile, level FROM autotile WHERE chipset = %d", $chipset['id'] )->rows();

	$form = $rc->get_edit($map['id']);
	$form->action .= '&edit='.$map['id'];
	$form->onsubmit = 'mapeditor.save( this );';

?><table>
	<tr>
		<td valign="top" width="400">
			<div class="well">
				<h1>Palette</h1>
				<div align="center" style="height: 512px; overflow: auto;">
				<canvas id="palette" onmousedown="selection.start()" onmousemove="selection.drag()" onmouseup="selection.stop()"></canvas>
				</div>
			</div>

			<div class="well">
				<h1>Karte bearbeiten</h1>
				<?=$form; ?>
			</div>
		</td><td valign="top">
			<div class="well">
				<h1>Karteneditor</h1>

				<div class="btn-toolbar">
					<div class="btn-group" data-toggle="buttons-radio">
						<a class="btn active" onclick="mapeditor.tool( 'pen' );">Pinsel</a>
						<a class="btn" onclick="mapeditor.tool( 'fill' );">Füllwerkzeug</a>
					</div>

					<div class="btn-group" data-toggle="buttons-checkbox">
						<a class="btn" onclick="mapeditor.toggleraster()">Raster</a>
					</div>

					<div class="btn-group" id="layerlist" data-toggle="buttons-radio">
						<a class="btn" href="javascript: mapeditor.setlayer('event');">Events</a>
						<a class="btn" href="javascript: mapeditor.addlayer();" id="addlayer"><i class="icon-plus"></i></a>
						<a class="btn" href="javascript: mapeditor.removelayer();" id="removelayer"><i class="icon-minus"></i></a>
					</div>
				</div>

				<div style="overflow: auto; margin: 10px; max-width: 1300px; max-height: 900px;">
				<canvas id="mapeditor" onmousedown="mapeditor.start()" onmousemove="mapeditor.drag()" onmouseup="mapeditor.stop()" style="background-color: lightgrey;"></canvas>
				</div><div id="coords"></div>
			</div>
		</td>
	</tr>
</table>

<div class="modal modal-events" id="event_editor">
	<div class="modal-header">
		<a class="close" data-dismiss="modal">×</a>
		<h3>Events</h3>
	</div>

	<div class="modal-body">
		<iframe id="event_frame" frameborder="0" style="width: 100%; height: 680px" src=""></iframe>
	</div>

	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Close</a>
		<a href="#" class="btn btn-primary">Save changes</a>
	</div>
</div>

<script type="text/javascript" src="js/editor/map.js"></script>
<script type="text/javascript">
	mapeditor.id = <?=json_encode($map['id']); ?>;
	mapeditor.w = <?=json_encode($map['width']); ?>;
	mapeditor.h = <?=json_encode($map['height']); ?>;
	mapeditor.data = <?=empty( $map['data'] ) ? '[[],[],[]]' : $map['data']; ?>;


	graphic.urls.push( [ 'event', 'img/icon/event.gif' ] );

	graphic.load( function() {
		mapeditor.chipset = palette.chipset = new chipset( <?=json_encode($chipset); ?>, function() {
			palette.canvas = document.getElementById( "palette" );
			palette.draw();

			mapeditor.canvas = document.getElementById( "mapeditor" );
			mapeditor.init();
			mapeditor.draw();

			$('#tool'+brush.type).addClass( 'active' );
		});
	});
</script><?php

}
