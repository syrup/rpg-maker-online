<?php

$tilesets = glob( 'img/tileset/*' );
$tilesets = array_combine( $tilesets, $tilesets );

$rc_chip = new data_controller( 'chipset', SELF );
$rc_chip->add( "name", "Name", 1, 1, 1, 1, "text" );
$rc_chip->add( "tileset", "Tileset", 1, 1, 1, 1, "select", $tilesets );
$rc_chip->add( "data", "Daten", 0, 0, 1, 0, "hidden" );
$rc_chip->prefix = 'chip_';

if( $rc_chip->run()) success( 'Änderungen erfolgreich gespeichert!' );

echo '<table><tr><td width="600" valign="top">';
echo '<div class="well"><h1>Chipsets</h1>';
echo $rc_chip->get_list();
echo '</div></td><td width="450" valign="top">';

if( !empty( $_GET['chip_edit'] )) {
	$chipset = $db->id_get( 'chipset', $_GET['chip_edit'] );
	$chipset['data'] = json_decode( $chipset['data'] );
	$chipset['autotile'] = $db->query( "SELECT autotile, level FROM autotile WHERE chipset = %d", $chipset['id'] )->rows();

	$autotiles = glob( 'img/autotile/*' );
	$autotiles = array_combine( $autotiles, $autotiles );

	$rc_auto = new data_controller( 'autotile', SELF.'&chip_edit='.$chipset['id']);
	$rc_auto->add( "autotile", "Autotile", 1, 1, 1, 1, "select", $autotiles );
	$rc_auto->add( "level", "Kollisionseinstellung", 1, 1, 1, 0, "select", array( "Below Hero", "Same Level as Hero", "Above Hero", "Rooftile" ));
	$rc_auto->auto['create'] = array( 'chipset' => $chipset['id']);
	$rc_auto->condition = 'chipset = '.$chipset['id'];
	$rc_auto->prefix = 'auto_';

	if( $rc_auto->run()) success( 'Änderungen erfolgreich gespeichert!' );

	$form = $rc_chip->get_edit( $chipset['id'] );
	$form->onsubmit = 'chipeditor.save( this );';
	$form->action .= '&chip_edit='.$chipset['id'];
	echo '<div class="well"><h1>Chipset bearbeiten</h1>'.$form.'</div>';

	echo '<div class="well"><h1>Autotiles</h1>';
	echo $rc_auto->get_form();
	echo $rc_auto->get_list();

?></div>
</td><td width="350" valign="top">
	<div class="well">
		<h1>Kollisionseinstellungen</h1>
		<div align="center" style="height: 384px; overflow: auto; margin: 4px;">
			<canvas onclick="chipeditor.change();" id="palette"></canvas>
		</div>
	</div>
<script type="text/javascript">
	var mouse = { x: 0, y: 0 };

	var chipeditor = {
		canvas: null,

		set: null,

		repaint: function() {
			this.set.display( this.canvas );
			this.set.collision( this.canvas );
		},

		change: function() {
			var x = Math.floor( mouse.x / config.tile.width );
			var y = Math.floor( mouse.y / config.tile.height );
			var i = x+y*8;

			chipeditor.set.change( i );
			chipeditor.repaint();
		},

		save: function( form ) {
			form['data'].value = this.set.data.toJSONString();
		}
	}

	document.onmousemove = function( ev ) {
		mouse.x = ev.layerX;
		mouse.y = ev.layerY;
	};

	graphic.urls.push( [ 'symbols', 'img/icon/symbols.gif' ] );

	graphic.load( function() {
		chipeditor.set = new chipset( <?=json_encode($chipset); ?>, function() {
			chipeditor.canvas = document.getElementById( "palette" );
			chipeditor.repaint();
		})
	});
</script></td><?php
} else {
	echo '<div class="well"><h1>Chipset erstellen</h1>'.$rc_chip->get_create().'</div>';
}

echo '</td></tr>';