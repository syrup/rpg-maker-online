<?php

if( isset( $_POST['source'] ))
	file_put_contents( 'data/global.js', $_POST['source'] );

?><script language="Javascript" type="text/javascript" src="extern/edit_area/edit_area_full.js"></script>
<script language="Javascript" type="text/javascript">

		editAreaLoader.init({
			id: "form_field_0"	// id of the textarea to transform
			,start_highlight: true	// if start with highlight
			,allow_resize: "both"
			,allow_toggle: false
			,word_wrap: false
			,language: "de"
			,syntax: "js"
		});

</script>

<div class="well">
	<h1>Globales Javascript</h1>
	<form method="post" action="<?= SELF; ?>">
		<textarea style="width: 100%;" rows="35" id="form_field_0" name="source"><?= htmlspecialchars( @file_get_contents( 'data/global.js')); ?></textarea>
		<div class="form_buttons"><input type="submit" value="Speichern"></div>
	</form>
</div>
