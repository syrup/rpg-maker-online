<?php


if( !empty( $_FILES['file'] )) {
	$name = $_FILES['file']['name'];
	$extension = strrchr( $_FILES['file']['name'], '.');

	if( $extension == '.ogg' ) {
		$path = "sound/$name";
		move_uploaded_file( $_FILES['file']['tmp_name'], $path );
		chmod( $path, 0664 );
		success( 'Upload erfolgreich' );
	} else error( 'Unerlaubte endung: '.$extension );
}

$res = glob( "sound/*" );

?><div class="well">
	<?php

		echo "<h1>Sounds</h1><div class=\"menu\">";

		foreach( $res as $file ) {
			echo "<a href=\"#\" onclick=\"sound.play( '".$file."?time=".time()."' ); return false;\">&raquo; $file</a>";
		}

	?></div>
</div>

<div class="well">
	<h1>Neue Ressource hinzufügen</h1>

	<?php

		$form = new form_renderer( SELF.'&type='.$show );
		$form->enctype = 'multipart/form-data';
		$form->text( 'file', 'Datei' )->input( 'type', 'file' );
		echo $form;

	?>
</div>
