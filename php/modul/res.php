<?php

$ress_types = array();
foreach( glob( 'img/*' ) as $folder)
	$ress_types[] = substr( $folder, 4 );

$show = in_array( $_GET['type'], $ress_types ) ? $_GET['type'] : $ress_types[0];


if( !empty( $_FILES['file'] )) {
	$name = $_FILES['file']['name'];
	$extension = strrchr( $_FILES['file']['name'], '.');

	if( $extension == '.png' || $extension == '.gif'  || $extension == '.jpg' ) {
		$path = "img/$show/$name";
		move_uploaded_file( $_FILES['file']['tmp_name'], $path );
		chmod( $path, 0664 );
		success( 'Upload erfolgreich' );
	} else error( 'Unerlaubte endung: '.$extension );
}

$res = glob( "img/$show/*" );

?><script language="javascript" type="text/javascript">
	leucht = null;
	function preview_ress( ele, file ) {
		document.getElementById( 'preview' ).src = 'img/icon/loading_klein.gif';
		document.getElementById( 'preview' ).src = file;
		if( leucht != null ) leucht.className = "";
		ele.className = "active";
		leucht = ele;
	}
</script>

<table>
	<tr>
		<td width="300" valign="top">
			<div class="well sidebar-nav">
				<h1>Ressourcen</h1>

				<ul class="nav nav-list"><?php

				foreach( $ress_types as $t ) {
					echo $t == $show ? '<li class="active">' :'<li>';
					echo '<a href="'.SELF.'&type='.$t.'">&raquo; '.ucfirst( $t ).'</a></li>';
				}

				?>
				</div>
			</div>
		</td><td valign="top" width="350">
			<div class="well">
				<h1>Vorschau</h1>
				<div style="width: 90%; margin: 10px auto; height: 500px; overflow: auto; border: 1px solid black"><img id="preview" alt="Vorschau" src="img/icon/blank.jpg"></div>
			</div>
		</td><td valign="top" width="600">
			<div class="well sidebar-nav">
				<?php

					echo "<h1>Ressourcen: $show</h1><div class=\"nav nav-list\">";

					foreach( $res as $file ) {
						echo "<li onclick=\"preview_ress( this, '".$file."?time=".time()."' );\"><a>&raquo; $file</a></li>";
					}

				?></div>
			</div>

			<div class="well">
				<h1>Neue Ressource hinzufügen</h1>

				<?php

					$form = new form_renderer( SELF.'&type='.$show );
					$form->enctype = 'multipart/form-data';
					$form->text( 'file', 'Datei' )->input( 'type', 'file' );
					echo $form;

				?>
			</div></td>
	</tr>
</table>