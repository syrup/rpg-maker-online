<?php

if (!isset($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_USER'] != 'inno' || $_SERVER['PHP_AUTH_PW'] != 'schaschlik' ) {
	header('WWW-Authenticate: Basic realm="My Realm"');
	header('HTTP/1.0 401 Unauthorized');
	echo 'Text, der gesendet wird, falls der Benutzer auf Abbrechen drückt';
	exit;
}

	require 'php/common.php';
	data_controller::$icon_dir = 'img/icon/';

	if( strchr( $_GET['site'], '.' ))
		throw new Exception( 'du bist ein sehr böser junge!' );

	$page = empty( $_GET['site'] ) ? 'index' : $_GET['site'];
	if( !is_file( "php/modul/$page.php" )) $page = 'index';

	$pages = array(
			'res' => 'Ressourcen',
			'chipset' => 'Chipsets',
			'maps' => 'Karten',
			'sounds' => 'Sound',
			'event' => 'Events',
			'global' => 'Globale Scripte'
	);

?><!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>RPG Maker Online</title>

		<link href="extern/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
		<link href="extern/bootstrap/css/bootstrap-responsive.css" rel="stylesheet" type="text/css">
		<link href="css/editor.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" src="js/general/config.js"></script>
		<script type="text/javascript" src="js/general/sound.js"></script>
		<script type="text/javascript" src="js/general/graphic.js"></script>
		<script type="text/javascript" src="js/general/mouse.js"></script>
		<script type="text/javascript" src="js/general/chipset.js"></script>
		<script type="text/javascript" src="js/general/ajax.js"></script>

		<script type="text/javascript" src="extern/json/json.js"></script>
		<script type="text/javascript" src="extern/jquery.min.js"></script>
		<script type="text/javascript" src="extern/bootstrap/js/bootstrap.min.js"></script>
	</head>
	<body>
		<?php if( !isset( $_GET['nomenu'] )) { ?>
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<a class="brand" href="editor.php">RPG Maker Online</a>

					<div class="nav-collapse">
						<ul class="nav"><?php
							foreach( $pages as $key => $value ) {
								echo $key == $page ? '<li class="active">' : '<li>';
								echo '<a href="editor.php?site='.$key.'">'.$value.'</a></li>';
							}
						?></ul>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>

		<div id="content" class="container"><?php

		try {
			define( 'SELF', 'editor.php?site='.$page );
			include "php/modul/$page.php";
		} catch( mysql_exception $e ) {
			error( $e->getMessage().'<br>'.$e->getSql().'<br>'.$e->getError());
		} catch( Exception $e ) {
			error( $e->getMessage());
		}

		?></div>
	</body>
</html>
